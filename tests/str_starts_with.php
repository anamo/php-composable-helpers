<?php declare (strict_types = 1);
use PHPUnit\Framework\TestCase;

final class str_starts_with extends TestCase
{

	public function teststr_starts_with1(): void
	{
		$this->assertEquals(true, str_starts_with('abc', ''));
	}

	public function teststr_starts_with2(): void
	{
		$this->assertEquals(true, str_starts_with('The lazy fox jumped over the fence', 'The'));
	}

	public function teststr_starts_with3(): void
	{
		$this->assertEquals(false, str_starts_with('The string starts with "the"', 'the'));
	}

	public function teststr_starts_with4(): void
	{
		$this->assertEquals(true, str_starts_with('Μα είναι δυνατόν;', 'Μα'));
	}

	public function teststr_starts_with5(): void
	{
		$this->assertEquals(false, str_starts_with('Μα είναι δυνατόν;', 'μα'));
	}
}
