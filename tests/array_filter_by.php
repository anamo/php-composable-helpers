<?php declare (strict_types = 1);
use PHPUnit\Framework\TestCase;

final class array_filter_by extends TestCase
{

	public function testarray_filter_by1(): void
	{
		$haystack = ['string_value'];
		$expected = ['string_value'];
		$result = array_filter_by($haystack, 'string_value');

		$this->assertSame($expected, $result);
	}

	public function testarray_filter_by2(): void
	{
		$haystack = [
			['name' => 'cook', 'done' => true],
			['name' => 'clean', 'done' => true],
			['name' => 'write more unit tests', 'done' => false]
		];
		$expected = [
			['name' => 'cook', 'done' => true]
		];
		$result = array_filter_by($haystack, 'name', 'cook');

		$this->assertSame($expected, $result);
	}

	public function testarray_filter_by3(): void
	{
		$haystack = [
			(object) ['name' => 'cook', 'done' => true],
			(object) ['name' => 'clean', 'done' => true],
			(object) ['name' => 'write more unit tests', 'done' => false]
		];
		$expected = [
			(object) ['name' => 'cook', 'done' => true]
		];
		$result = array_filter_by($haystack, 'name', 'cook');

		$this->assertEquals($expected, $result);
	}

	public function testarray_filter_by4(): void
	{
		$haystack = [
			new class

		{
				public function math($a, $b): int
			{
					return $a + $b;
				}
			},
			new class

		{
				public function math($a, $b): int
			{
					return $a - $b;
				}
			},
			new class

		{
				public function math($a, $b): int
			{
					return $a / $b;
				}
			}
		];
		$expected = 1;
		$result = array_filter_by($haystack, 'math', 20, 10, 10);

		$this->assertEquals($expected, count($result));
	}
}
