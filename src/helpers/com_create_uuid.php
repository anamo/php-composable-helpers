<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Generate a globally unique identifier which consists only of numbers (UUID)
 * Similar to com_create_guid but takes a range of chars. By default CHAR(30) starting with 1
 *
 * > com_create_uuid();
 * > com_create_uuid('1000000000', '9999999999'));
 * > com_create_uuid('100000000000000000000000000000', '999999999999999999999999999999'));
 *
 */
if (!function_exists('com_create_uuid')) {
	function com_create_uuid(string $min = '100000000000000000000000000000', string $max = '999999999999999999999999999999'): string
	{
		$range = gmp_sub($max, $min);
		$random = gmp_random();
		$random = gmp_mod($random, $range);
		$random = gmp_add($min, $random);
		return str_pad($random, strlen($max), '0');
	}
}
