<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * An easy-to-use wrapper for the curl_multi native functions. It uses a key-value array to
 * store multiple curl_requests and immediately returns the responses.
 * If $request_callback returns null | void the payload is left intact to be executed via curl.
 *
 * > curl_multi(['promise1' => [CURLOPT_URL => 'https://pastebin.com']]);
 * > curl_multi(['promise1' => [CURLOPT_URL => '/contacts']], ['base_url' => 'https://pastebin.com']);
 * > curl_multi(['promise1' => [CURLOPT_URL => 'https://pastebin.com']], [CURLOPT_TIMEOUT => 4]);
 * > curl_multi(['promise1' => [CURLOPT_URL => 'https://pastebin.com']], [CURLOPT_TIMEOUT => 4], 'json_decode');
 * > curl_multi(['promise1' => [CURLOPT_URL => 'https://pastebin.com']], [CURLOPT_TIMEOUT => 4], function($v) { return json_decode($v, true); });
 * > curl_multi(['promise1' => [CURLOPT_URL => 'https://pastebin.com']], [CURLOPT_TIMEOUT => 4], function($v) { return json_decode($v, true); }, function($v) { return '{"id":"1111111111"}'; });
 *
 * returns [
 * 'promise1' => [
 * 		'response' => string | mixed from response_callback
 * 		'httpcode' => int | always 200 if request_callback is set
 * 	]
 * ]
 */
if (!function_exists('curl_multi')) {
	function curl_multi(array $promises, array $curl_defaults = [], callable $response_callback = null, callable $request_callback = null): array
	{
		$mh = curl_multi_init();
		$chs = [];

		$promises = curl_multi_getopt($promises, $curl_defaults);

		$cached_responses = [];

		if (!is_null($request_callback)) {
			array_walk($promises, function (&$v, $k) use (&$promises, &$cached_responses, &$request_callback) {
				if (array_key_exists(CURLOPT_POST, $v) &&
					$v[CURLOPT_POST]) {
					return;
				}
				if (array_key_exists(CURLOPT_POSTFIELDS, $v)) {
					return;
				}
				if (array_key_exists(CURLOPT_CUSTOMREQUEST, $v) &&
					'GET' != strtoupper(trim($v[CURLOPT_CUSTOMREQUEST]))) {
					return;
				}

				if (is_null($result = call_user_func($request_callback, $v))) {
					return;
				}

				$cached_responses[$k] = $result;
				unset($promises[$k]);
			});
		}

		foreach ($promises as $k => $v) {
			curl_setopt_array($ch = curl_init(), $v);
			$chs[$k] = $ch;
			curl_multi_add_handle($mh, $ch);
		}

		do {
			$status = curl_multi_exec($mh, $active);
			if ($active) {
				curl_multi_select($mh);
			}
		} while ($active &&
			CURLM_OK == $status);

		foreach ($chs as $ch) {
			curl_multi_remove_handle($mh, $ch);
		}

		curl_multi_close($mh);

		return array_merge(
			array_map(fn($v) => [
				'response' => !is_null($response_callback) ? call_user_func($response_callback, $v) : $v,
				'httpcode' => 200
			], $cached_responses),
			array_map(fn($v) => [
				'response' => !is_null($response_callback) ? call_user_func($response_callback, curl_multi_getcontent($v)) : curl_multi_getcontent($v),
				'httpcode' => curl_getinfo($v, CURLINFO_HTTP_CODE)
			], $chs)
		);
	}
}
