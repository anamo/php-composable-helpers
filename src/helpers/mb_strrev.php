<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * PHP's strrev is not safe to use on utf-8 strings because it reverses a string one byte at a time. So if a character consists of multiple bytes it cannot be preserved as an entity in the reversed result.
 * >	echo    strrev('Gonçalves') . "\n"; // returns sevla??noG
 * >	echo mb_strrev('Gonçalves') . "\n"; // returns sevlaçnoG
 */
if (!function_exists('mb_strrev')) {
	function mb_strrev($string, $encoding = null)
	{
		if (null === $encoding) {
			$encoding = mb_detect_encoding($string);
		}

		$length = mb_strlen($string, $encoding);
		$reversed = '';
		while ($length-- > 0) {
			$reversed .= mb_substr($string, $length, 1, $encoding);
		}

		return $reversed;
	}
}
