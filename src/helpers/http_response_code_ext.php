<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * https://en.wikipedia.org/wiki/List_of_HTTP_status_codes.
 * >	http_response_code_ext(520);
 */
if (!function_exists('http_response_code_ext')) {
	function http_response_code_ext(int $code): void
	{
		if (http_response_code($code) != $code) {
			switch ($code) {
				case 520:$text = 'Web Server Returned an Unknown Error';
					break;
				case 521:$text = 'Web Server Is Down';
					break;
				case 522:$text = 'Connection Timed Out';
					break;
				case 523:$text = 'Origin Is Unreachable';
					break;
				case 524:$text = 'A Timeout Occurred';
					break;
				case 525:$text = 'SSL Handshake Failed';
					break;
				case 526:$text = 'Invalid SSL Certificate';
					break;
				default:$text = 'Unknown HTTP status code "'.$code.'"';
					break;
			}

			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

			header("$protocol $code $text");
		}
	}
}
