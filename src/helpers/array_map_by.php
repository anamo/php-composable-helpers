<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Similari to array_map this specialized function returns the value of the named property on all items in the enumeration.
 *
 * > array_map_by('object_prop', $haystack));
 * > array_map_by('array_key', $haystack));
 * > array_map_by('function_name', $haystack, 'function_arg1', 'function_arg2'));
 *
 */
if (!function_exists('array_map_by')) {
	function array_map_by(string $prop, array $haystack, ...$all_the_others): array
	{
		return array_map(function ($v) use ($prop, &$all_the_others) {
			return is_object($v) ? (method_exists($v, $prop) ? call_user_func([$v, $prop], $all_the_others) : $v->{$prop}) : $v[$prop];
		}, $haystack);
	}
}
