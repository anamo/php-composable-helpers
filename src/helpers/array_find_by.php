<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Finds the elements of an array which satisfy the filters. The original array is not affected.
 * Similar to array_filter but takes an array instead of a function.
 *
 * > array_find_by($haystack, 'string_value'));
 * > array_find_by($haystack, 'object_prop', 3355));
 * > array_find_by($haystack, 'array_key', 3355));
 * > array_find_by($haystack, 'function_name', 'function_arg1', 'function_arg2'));
 *
 */
if (!function_exists('array_find_by')) {
	function array_find_by(...$args)
	{
		return reset(array_filter_by(...$args));
	}
}
