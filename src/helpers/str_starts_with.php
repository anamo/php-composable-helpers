<?php /*! market-support-website v5.0.0 | © 2006-present Panagiotis Tsimpoglou. All rights reserved | bitbucket.org/anamo/market-support-website */

/**
 * https://www.php.net/manual/en/function.str-starts-with.php#125913
 * Checks if a string starts with a given substring.
 * Performs a case-sensitive check indicating if haystack begins with needle.
 */
if (!function_exists('str_starts_with')) {
	function str_starts_with(string $haystack, string $needle): bool
	{
		$needle_len = mb_strlen($needle);
		return 0 === $needle_len ||
		0 === @substr_compare($haystack, $needle, 0, $needle_len);
	}
}
