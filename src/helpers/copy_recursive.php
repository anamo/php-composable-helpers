<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 *	Recursively copy dir.
 */
if (!function_exists('copy_recursive')) {
	function copy_recursive(string $source, string $dest): void
	{
		$dir = opendir($source);
		@mkdir($dest);
		while (false !== ($file = readdir($dir))) {
			if (('.' != $file) &&
				('..' != $file)) {
				if (is_dir($source.'/'.$file)) {
					copy_recursive($source.'/'.$file, $dest.'/'.$file);
				} else {
					copy($source.'/'.$file, $dest.'/'.$file);
				}
			}
		}
		closedir($dir);
	}
}
