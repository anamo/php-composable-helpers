<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Finds the elements of an array which does not satisfy the filters. The original array is not affected.
 * Similar to array_filter but takes an array instead of a function.
 *
 * > array_group_by($haystack, 'string_value'));
 * > array_group_by($haystack, 'object_prop'));
 * > array_group_by($haystack, 'array_key'));
 * > array_group_by($haystack, 'function_name', 'function_arg1', 'function_arg2'));
 *
 */
if (!function_exists('array_group_by')) {
	function array_group_by(array $haystack, string $prop, ...$all_the_others): array
	{
		$groupedContent = [];
		foreach ($haystack as $item) {
			if (is_object($item)) {
				if (method_exists($item, $prop)) {
					$groupedContent[call_user_func_array([$item, $prop], $all_the_others)][] = $item;
				} else {
					$groupedContent[$item->{$prop}][] = $item;
				}
			} else {
				$groupedContent[$item[$prop]][] = $item;
			}
		}
		return $groupedContent;
	}
}
