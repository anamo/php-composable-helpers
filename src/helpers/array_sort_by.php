<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Sorts an array by a property. The original array is not affected.
 * Similar to array_filter but takes an array instead of a function.
 *
 * > array_sort_by($haystack, 'name'));
 * > array_sort_by($haystack, function($a, $b) { return strcmp($a->name, $b->name); }));
 *
 */
if (!function_exists('array_sort_by')) {
	function array_sort_by(array $haystack, $prop, ...$all_the_others): array
	{
		$new_array = $haystack;
		if (is_string($prop)) {
			usort($new_array, function ($a, $b) use (&$prop, &$all_the_others) {
				if (is_object($a) &&
					is_object($b)) {
					if (method_exists($a, $prop) &&
						method_exists($b, $prop)) {
						return strcmp(call_user_func_array([$a, $prop], $all_the_others), call_user_func_array([$b, $prop], $all_the_others));
					} else {
						return strcmp($a->{$prop}, $b->{$prop});
					}
				} else {
					return strcmp($a[$prop], $b[$prop]);
				}
			});
		} else if (is_callable($prop)) {
			usort($new_array, $prop);
		}
		return $new_array;
	}
}
