<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * https://www.php.net/manual/en/function.str-contains.php#125977
 * Determine if a string contains a given substring.
 * Performs a case-sensitive check indicating if needle is contained in haystack.
 * Returns true if needle is in haystack, false otherwise.
 * Checking the existence of the empty string will always return true.
 */
if (!function_exists('str_contains')) {
	function str_contains(string $haystack, string $needle): bool
	{
		return '' !== $needle &&
		false !== mb_strpos($haystack, $needle);
	}
}
