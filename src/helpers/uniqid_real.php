<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 *	Generate a unique ID.
 *
 *	> uniqid_real();
 *	2342cabf2ef9b96
 *	> uniqid_real(30);
 * 32264257da7282449f71b52c50cc55
 */
if (!function_exists('uniqid_real')) {
	function uniqid_real(int $length = 15): string
	{
		if (function_exists('\random_bytes')) {
			$str = substr(bin2hex(\random_bytes(ceil($length / 2))), 0, $length);

		} elseif (function_exists('\openssl_random_pseudo_bytes')) {
			$str = substr(bin2hex(\openssl_random_pseudo_bytes(ceil($length / 2))), 0, $length);

		} elseif (function_exists('\mcrypt_create_iv')) {
			$str = substr(bin2hex(\mcrypt_create_iv(ceil($length / 2), MCRYPT_DEV_URANDOM)), 0, $length);

		} else {
			$str = substr(uniqid('', true), 0, $length); // maxlength 24 chars
		}
		for ($i = 0, $c = strlen($str); $i < $c; $i++) {
			$str[$i] = rand(0, 100) > 50 ? strtoupper($str[$i]) : $str[$i];
		}

		return $str;
	}
}
