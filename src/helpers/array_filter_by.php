<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Finds the elements of an array which satisfy the filters. The original array is not affected.
 * Similar to array_filter but takes an array instead of a function.
 *
 * > array_filter_by($haystack, 'string_value'));
 * > array_filter_by($haystack, 'object_prop', 3355));
 * > array_filter_by($haystack, 'array_key', 3355));
 * > array_filter_by($haystack, 'function_name', 'function_arg1', 'function_arg2'));
 *
 */
if (!function_exists('array_filter_by')) {
	function array_filter_by(array $haystack, string $prop, $is = null, ...$all_the_others): array
	{
		if (2 == func_num_args()) {
			return array_filter($haystack, function ($v) use (&$prop) {
				return $v == $prop;
			});
		}
		return array_filter($haystack, function ($v) use (&$prop, &$is, &$all_the_others) {
			return (is_object($v) ? (method_exists($v, $prop) ? call_user_func_array([$v, $prop], $all_the_others) : $v->{$prop}) : $v[$prop]) == $is;
		});
	}
}
