<?php /*! anamo/php-composable-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/php-composable-helpers */

/**
 * Returns an array of the all curl_opts ready to be used in the curl_set_opt_array part of the curl_multi function.
 * The main purpose of this function is to provide an easy way to figure the final opt_array used by the curl_multi
 * function, so that we can easily cache the responses of the curl_multi.
 *
 * > curl_multi_getopt(['promise1' => [CURLOPT_URL => 'https://pastebin.com']]);
 * > curl_multi_getopt(['promise1' => [CURLOPT_URL => '/contacts']], ['base_url' => 'https://pastebin.com']);
 * > curl_multi_getopt(['promise1' => [CURLOPT_URL => 'https://pastebin.com']], [CURLOPT_TIMEOUT => 4]);
 *
 * returns [
 * 'promise1' => [
 * 		CURLOPT_URL => 'https://pastebin.com/contacts'
 * 		CURLOPT_TIMEOUT => 1,
 * 		CURLOPT_RETURNTRANSFER => 1,
 * 		CURLOPT_HTTPHEADER => array,
 * 		...
 * 	]
 * ]
 */
if (!function_exists('curl_multi_getopt')) {
	function curl_multi_getopt(array $promises, array $curl_defaults = []): array
	{
		$base_url_handler = null;
		if (array_key_exists('base_url', $curl_defaults)) {
			$base_url_handler = $curl_defaults['base_url'];
			unset($curl_defaults['base_url']);
		}

		array_walk($promises, function (&$v) use (&$base_url_handler, &$curl_defaults) {
			$handler = [];
			if ($base_url_handler) {
				$handler[CURLOPT_URL] = $base_url_handler.$v[CURLOPT_URL];
			}
			$v = array_replace_recursive((array) $v, $curl_defaults, $handler);
		});

		return $promises;
	}
}
