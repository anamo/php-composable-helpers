# anamo/php-composable-helpers

**Composable helpers for declarative coding in PHP.**

```shell

$ composer require anamo/php-composable-helpers

```
